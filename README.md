# Quick start

~~~~ {.bash}
~/workspace $ git clone <URL-to-repo>
~/workspace $ cd <repo>

~/workspace/v4 (master) $ npm install //set express app
~/workspace/v4 (master) $ npm start   //host app, or use nodemon - installed globally!
~~~~


## Heroku
Before running 

`heroku login -i`

we need to set up Heroku CLI for our workspace by running

~~~~ {.bash}
~/workspace $ curl https://cli-assets.heroku.com/install.sh | sh
~/workspace $ sudo ln -s /usr/local/bin/heroku /usr/bin/heroku
~/workspace $ cd <repo>
~~~~

After that, you can test if CLI is instaled properly by running

` heroku local`

which will serve your app locallz, just as with `npm start`.

## MongoDB

Be careful to first create and serve local database before accesing it!

Import prepared data from JSON file with the following:
~~~~ {.bash}
> mongoimport --db Comments --collection Comments --mode upsert --upsertFields name --jsonArray --file ~/workspace/v4/app_server/models/comments-mongodb.json
~~~~
## The trial database links

URI:
`mongodb://user:password1@ds115874.mlab.com:15874/comments`

in shell:
`mongo ds115874.mlab.com:15874/comments -u user -p password1`

//Then we add the database to the Herook environment parameters
`heroku config:set MLAB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments`

### Transfer data from local database into mLab

1.  Store local database data 
`mongodump -h localhost:27017 -d Comments -o ~/workspace/mongodb/dump`

2. Transfer data to mlab
`mongorestore -h ds115874.mlab.com:15874 -d comments -u user ~/workspace/mongodb/dump/Comments`

### Set Heroku on production
`heroku config:set NODE_ENV=production`

### Set MLAB_URI parameter for Heroku
`heroku config:set MLAB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments`

If following V4 instructions in a right manner, this will set that on Heroku, we are getting database data from mLab database.


### Test production mLab locally
`DB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments nodemon`

### Test production mLab on heroku 

### Heroku debug
`heroku logs --tail`

## Appendix 1
~~~
var mongoose = require('mongoose');
var Comment = mongoose.model('Comment');//model name, the same as for 

var JSONcallback = function(res, status, msg) {
  res.status(status);
  res.json(msg);
};

module.exports.getAll = function(req, res) {
    Comment.find()
    .exec(function(err, comment){
        
        JSONcallback(res, 200, comment);
        
    });
};
~~~

## Appendix 2

To disable horizontal scrolling, try adding following style
~~~~ {.css}
max-width: 100%;
overflow-x: hidden;
~~~~
to html and body tags.

